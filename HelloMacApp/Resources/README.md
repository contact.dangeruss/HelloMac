#  Introduction

Mac001 is my first ever mac app using visual studio 2019 on a MacBook Air M1. It has been some 7 years or so since I stopped developing and focussed on computer systems management. Things were different back then developing in Visual Studio 2015 on Windows having not long moved over from legacy Vb6 apps. However, the push to devops with in our systems team has brought be full circle to operating within a development paradigm. The purchase of this my first macbook was a timely excuse to dust off the old programming "skills" and learn something from scratch again. 

This project was created using the microsoft [Hello-Mac](https://docs.microsoft.com/en-gb/xamarin/mac/get-started/hello-mac) tutorial


