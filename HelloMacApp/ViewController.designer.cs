// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Mac001
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSButtonCell ButtonClicker { get; set; }

		[Outlet]
		AppKit.NSTextFieldCell ClickedLabel { get; set; }

		[Outlet]
		AppKit.NSLevelIndicatorCell ClickIndicator { get; set; }

		[Action ("ClickedButton:")]
		partial void ClickedButton (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ClickedLabel != null) {
				ClickedLabel.Dispose ();
				ClickedLabel = null;
			}

			if (ClickIndicator != null) {
				ClickIndicator.Dispose ();
				ClickIndicator = null;
			}

			if (ButtonClicker != null) {
				ButtonClicker.Dispose ();
				ButtonClicker = null;
			}
		}
	}
}
