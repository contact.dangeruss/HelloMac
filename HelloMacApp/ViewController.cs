﻿using System;

using AppKit;
using Foundation;
using System.Threading;

namespace Mac001
{
    public partial class ViewController : NSViewController
    {
        
        private Clicker theClicker;
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            theClicker = new Clicker(10,8);
            theClicker.TempChanged += TempChangeHanlder;

            // Do any additional setup after loading the view.
            ClickedLabel.StringValue = "Click the button as many times as possible.";
            ClickIndicator.DoubleValue = 0;


        }

        public void TempChangeHanlder(object sender, EventArgs e)
            {
            
            if (theClicker.Temperature > 0)
            {
                // Reset the counter and signal the waiting thread.

                BeginInvokeOnMainThread(() =>
                {
                    ButtonClicker.Enabled = theClicker.Enabled;
                    ClickIndicator.DoubleValue = theClicker.Temperature;
                    if (theClicker.Enabled)
                    { 
                        ClickedLabel.StringValue = string.Format("The button has been clicked {0} time{1}.", theClicker.ClickCount, (theClicker.ClickCount < 2) ? "" : "s");
                    }
                });
            }
        }
        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }

        partial void ClickedButton(NSObject sender)
        {
            try
            {
                theClicker.Invoke();
                //ClickedLabel.StringValue = string.Format("The button has been clicked {0} time{1}.", theClicker.ClickCount, (theClicker.ClickCount < 2) ? "" : "s");
               // ClickIndicator.DoubleValue = theClicker.Temperature;
            }
            catch (Exception ex)
            {
                ClickedLabel.StringValue = "Woah, Easy Tiger! " + ex.Message;
                /*
                var alert = new NSAlert()
                {
                    AlertStyle = NSAlertStyle.Critical,
                    InformativeText = "Woah, Easy Tiger! " + ex.Message,
                    MessageText = "KaBOOM!",
                };
                alert.RunModal();
                */
            }
        }
    }
}
