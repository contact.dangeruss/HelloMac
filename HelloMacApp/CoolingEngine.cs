﻿using System;
using System.Threading;

namespace Mac001
{
    public class CoolingEngine
    {
        public enum Rating
        {
            Basic,
            Stage1,
            Stage2,
            Racing
        }
        private Timer TempTimer;
        public event EventHandler Cool;
        public CoolingEngine(Rating spec)
        {
            var autoEvent = new AutoResetEvent(false);
            switch (spec)
            {
                default:
                    TempTimer = new Timer(Coolinterval, autoEvent, 1000, 1000);
                    break;
            }
        }

        private void Coolinterval(Object stateInfo)
        {
            AutoResetEvent autoEvent = (AutoResetEvent)stateInfo;
            this.OnCool(new EventArgs());
            autoEvent.Set();
        }

        protected virtual void OnCool(EventArgs e)
        {
            EventHandler handler = Cool;
            handler?.Invoke(this, e);
        }
    }
}
