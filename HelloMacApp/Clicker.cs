﻿using System;
namespace Mac001
{
    public class Clicker
    {
        private int _numberOfTimesClicked = 0;
        private int _clickerTemp = 0;
        private int _maxTemp = 10;
        private int _warningTemp = 8;
        private bool _enabled = true;
        private CoolingEngine Cooler;
        public event EventHandler TempChanged;

        public Clicker(int maxtemp, int warningtemp)
        {
            _maxTemp = maxtemp;
            if (warningtemp <= 0 || warningtemp >= maxtemp)
            {
                throw new Exception("warning temp must be greater than 0 and less than maxtemp");
            }
            else
            {
                _warningTemp = warningtemp;
            }
            this.addCooler();
            this.Cooler.Cool += CoolingHandler;
            Enabled = _enabled;
        }

        public void addCooler()
        {
            Cooler = new CoolingEngine(CoolingEngine.Rating.Basic);
        }
        public void Invoke()
        {
            if (_clickerTemp >= _maxTemp)
            {
                this.Enabled = false; 
                throw new Exception("You burned out the clicker, you will need to let it cool down!");
            }
            ++_numberOfTimesClicked;
            ++this.Temperature;
        }
        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
            }
        }
        public int ClickCount
        {
            get => _numberOfTimesClicked;
        }
        public int Temperature
        {
            get => _clickerTemp;
            set
            {
                _clickerTemp = value;
                OnTempChanged(new EventArgs());
            }
        }
        protected virtual void OnTempChanged(EventArgs e)
        {
            EventHandler handler = TempChanged;
            handler?.Invoke(this, e);
        }
        void CoolingHandler(object sender, EventArgs e)
        {
            if (_clickerTemp > 0)
            {
                // Reduce Temperature
                --this.Temperature;
                //Enable Clicker once cool enough
                if ((_clickerTemp < _warningTemp) &&(this.Enabled == false))
                {
                    this.Enabled = true;
                }
            }
        }
    }
}
