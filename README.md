# Introduction

This is my first ever app for MacOS, prompted by the purchase of my nice new shiny MacBook Air M1 (my first ever mac).
It has been a while since I have developed any software (6 Years ago in vb.net), so this cross platform, MVC based app, writen in C# on MacOS pretty much renderers me a newbie.


# Features

This app was based on the microsoft [Hello-Mac]() tutorial which guides you through the creation of a xamarin app with a button and label that reports the number of times it was clicked along with some basic UI constraints.

1. Button with Label that reports number of clicks
2. Button Limit with Error Handler to catch when the limit has been exceeded.
3. Indicator Control to show how near the limit the user is.
4. Cool Down timer that restores the button to a working state.

# Things I have learned 💡

Comming from a Vb6 Background with only a short spell in Vb.net and it being some 7 years since developing software I am basically starting from scratch. So many of the lessons learned are very much back-to-basics.

* Basic Csharp syntax 
* Basic UI controls using xamarin/xcode helpers
* Basic understanding of Model, View, Controller see Issue #1
* Basic event handling and threading using timer

# What is Next 🤔

To continue learning, I shall be adding features or otherwise just "flexing" integrations, extensions or UI designs. e.g

* Add Authentication Flow
* Add Cloud Document/Storage (Icloud)
* Work out how to build and deploy the app
* See if I can get it to work on Android.
* Seri Log and seq server
* Dapper or similar and a database engine. Save my model to a database somehow?
